import React, { useState } from "react";
import { render } from "react-dom";
import Scalable from "../../src/Scalable";

const isDevelopment = () => process.env.NODE_ENV !== 'production';

const MyPinchZoom = (props) => {
  const [transform, setTransform] = useState(null);
  return (
    <Scalable {...props} transform={transform} onTransformChange={(transform) => { console.log('transform', transform); setTransform(transform); }} />
  );
};

const SizedContainerView = ({menu, width, height, children }) => {
    const imageWidth = width * 2;
    const imageHeight = height * 2;
    return (
        <div>
            <nav>{menu}</nav>
            <main style={{ width: `${width}px`, height: `${height}px` }}>
                <MyPinchZoom doubleTapBehavior='zoom' debug={isDevelopment()}>
                    <img alt='Demo Image' src={`http://picsum.photos/${imageWidth}/${imageHeight}?random`} />
                </MyPinchZoom>
            </main>
        </div>
    );
}

const CenteredView = ({menu, width, height, imageWidth, imageHeight}) => {
  return (
    <div>
      <nav>{menu}</nav>
      <main style={{ width: `${width}px`, height: `${height}px` }}>
        <MyPinchZoom doubleTapBehavior='zoom' position='center' initialScale={1} minScale={1} maxScale={4} debug={isDevelopment()}>
          <img alt='Demo Image' src={`http://picsum.photos/${imageWidth}/${imageHeight}?random`} />
        </MyPinchZoom>
      </main>
    </div>
  );
}

const AnyContentView = ({menu, width, height, imageWidth, imageHeight}) => {
  return (
    <div>
      <nav>{menu}</nav>
      <main style={{ width: `${width}px`, height: `${height}px` }}>
        <MyPinchZoom maxScale={2} doubleTapBehavior='zoom' position='center' debug={isDevelopment()}>
          <div><span>Hello</span> <span>World</span> <input type="text" defaultValue="asd" /></div>
        </MyPinchZoom>
      </main>
    </div>
  );
}

const flexContentStyle = {
    fontSize: 20,
    margin: 'auto',
}
const FlexContainerView = ({menu}) => (
    <div style={{height: '100vh', display: 'flex', flexDirection: 'column'}}>
        <nav style={{flex: 'none'}}>{menu}</nav>
        <div style={{flex: 'none', textAlign: 'center'}}><span style={flexContentStyle}>The image fills 100% of the flex item in which it is contained</span></div>
        <main style={{flex: 'auto', overflow: 'hidden', display: 'flex'}}>
            <div style={{flex: 'none', alignSelf: 'center'}}>
                <span style={flexContentStyle}>Sidebar</span>
            </div>
            <div style={{flex: 'auto', overflow: 'hidden', position: 'relative'}}>
                <div style={{position: 'absolute', height: '100%', width: '100%'}}>
                    <MyPinchZoom
                      debug={isDevelopment()}
                      position='center'

                    >
                        <img alt='Demo Image' src='http://picsum.photos/2560/1440?random' />
                    </MyPinchZoom>
                </div>
            </div>
        </main>
    </div>
)

const Menu = ({viewId, onViewChange}) => {
    const getLinkStyle = linkViewId => {
        return {
            padding: 10,
            color: viewId === linkViewId
                ? 'orange'
                : 'blue',
        };
    };

    return (
        <React.Fragment>
            <span style={{fontSize: 20, fontWeight: 'bold', padding: 10}}>Demo</span>
            <a href='#' onClick={() => onViewChange(0)} style={getLinkStyle(0)}>Small</a>
            <a href='#' onClick={() => onViewChange(1)} style={getLinkStyle(1)}>Medium</a>
            <a href='#' onClick={() => onViewChange(3)} style={getLinkStyle(3)}>Centered</a>
            <a href='#' onClick={() => onViewChange(2)} style={getLinkStyle(2)}>Full-screen Flex</a>
            <a href='#' onClick={() => onViewChange(4)} style={getLinkStyle(4)}>Any Content</a>
        </React.Fragment>
    );
}

class App extends React.Component {
    state = {
        viewId: 0
    }
    
    handleViewChange = viewId => {
        this.setState({
            viewId
        });
    }

    render() {
        const { viewId } = this.state;
        const menu = <Menu viewId={viewId} onViewChange={this.handleViewChange} />
        return (
            viewId === 4 ? <AnyContentView menu={menu} />
            : viewId === 2 ? <FlexContainerView menu={menu} />
            : viewId === 3 ? <CenteredView menu={menu} width={300} height={500} imageWidth={200} imageHeight={400} />
            : viewId === 1 ? <SizedContainerView menu={menu} width={500} height={800} />
            : <SizedContainerView menu={menu} width={300} height={500} />
        );
    }
}

render(<App />, document.getElementById("root"));