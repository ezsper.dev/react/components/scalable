# react-scalable

A React component that adds pinch-zoom and pan capability to an `img` element or whatever. Both mobile and desktop browsers are supported. In desktop mode, you zoom with the mouse scrollwheel, and pan by dragging.

On render, the zoom and pan values are applied using CSS transforms. 

## Install

`npm install @ezsper/react-scallable --save`

## Try it out

### Online

Comming Soon

### Local

1. `git clone https://gitlab.com/ezsper.dev/react/components/scalable.git react-scalable`
2. `cd react-scalable`
3. `npm install`
4. `npm start`
5. Browse to http://localhost:3001

## Usage

```javascript
import React from "react";
import Scalable from "@ezsper/react-scalable";

const App = () => {
    return (
        <div style={{ width: '500px', height: '500px' }}>
            <Scalable>
                <img alt='Test Image' src='http://picsum.photos/750/750' />
            </Scalable>
        </div>
    );
};
```

## API

Prop		| Type		| Default	| Description
------------|-----------|-----------|--------------------------------------------------------------------------------------------------------------------
initialScale| number	| null	    | The initial scale of the image.  When `null`, the image will be proportionally 'autofit' to the container.
initialTop  | number	| null	    | The initial top of the image.  
initialLeft | number	| null	    | The initial left of the image. 
minScale	| number	| null	    | The minimum scale to which the image can be zoomed out. When `null`, the minimum scale is the 'autofit' scale.
maxScale	| number	| 1			| The maximum scale to which the image can be zoomed in. 
position    | 'center' or 'topLeft'    | 'topLeft'  | Position of the image relative to the container. Applies when the scaled image is smaller than the container.
doubleTapBehavior	| 'reset' or 'zoom' | 'reset'		| Whether to zoom in or reset to initial scale on double-click / double-tap.

## Development

To set up the project:

1.  Fork and clone the repository
2.  `npm install`
3.  `npm start`

The example page will be available on http://localhost:3001 in watch mode, meaning you don't have to refresh the page to see your changes.

## Roadmap

* On scale down it should not change top left if out of bounds