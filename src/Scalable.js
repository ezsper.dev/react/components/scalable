import React from 'react';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
import warning from 'warning';
import DebugView from './StateDebugView';

import { snapToTarget, negate, constrain, getPinchLength, getPinchMidpoint, getRelativePosition, setRef, isEqualDimensions, getDimensions, getContainerDimensions, isEqualTransform, getAutofitScale, getMinScale, tryCancelEvent, getImageOverflow } from './Utils';

const OVERZOOM_TOLERANCE = 0.05;
const DOUBLE_TAP_THRESHOLD = 250;
const ANIMATION_SPEED = 0.1;

const isInitialized = (top, left, scale) => scale !== undefined && left !== undefined && top !== undefined;

const imageStyle = createSelector(
    state => state.top,
    state => state.left,
    state => state.scale,
    (top, left, scale) => {
        const style = {};
        return isInitialized(top, left, scale)
            ? {
                ...style,
                transform: `translate3d(${left}px, ${top}px, 0) scale(${scale})`,
                transformOrigin: '0 0',
                float: 'left',
            } : style;
    }
);

const imageOverflow = createSelector(
    (state, transform) => transform.top,
    (state, transform) => transform.left,
    (state, transform) => transform.scale,
    state => state.imageDimensions,
    state => state.containerDimensions,
    (top, left, scale, imageDimensions, containerDimensions) => { 
        if (!isInitialized(top, left, scale)) {
            return '';
        } 
        return getImageOverflow(top, left, scale, imageDimensions, containerDimensions);
    }
);

const browserPanActions = createSelector(
    imageOverflow,
    (imageOverflow) => { 
        //Determine the panning directions where there is no image overflow and let
        //the browser handle those directions (e.g., scroll viewport if possible).
        //Need to replace 'pan-left pan-right' with 'pan-x', etc. otherwise 
        //it is rejected (o_O), therefore explicitly handle each combination.
        const browserPanX = 
            !imageOverflow.left && !imageOverflow.right ? 'pan-x' //we can't pan the image horizontally, let the browser take it
            : !imageOverflow.left ? 'pan-left'
            : !imageOverflow.right ? 'pan-right'
            : '';
        const browserPanY = 
            !imageOverflow.top && !imageOverflow.bottom ? 'pan-y'
            : !imageOverflow.top ? 'pan-up'
            : !imageOverflow.bottom ? 'pan-down'
            : '';
        return [browserPanX, browserPanY].join(' ').trim();
    }
);

const defaultShouldDisableTouch = event => {
   const isTouchDevice = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));
   const tagTouchDisableRegExp = new RegExp(`^(${!isTouchDevice ? 'SPAN|': ''}BUTTON|A|INPUT|TEXTAREA|SELECT)$`);
    let disable = event.target.tagName.match(tagTouchDisableRegExp);
    if (!disable) {
      const value = event.target.getAttribute('data-user-scale');
      if (value === 'false') {
        disable = true;
      }
    }
    return disable;
}

//Ensure the image is not over-panned, and not over- or under-scaled.
//These constraints must be checked when image changes, and when container is resized.
export class Scalable extends React.Component {
    state = {};

    lastPointerUpTimeStamp; //enables detecting double-tap
    lastPanPointerPosition; //helps determine how far to pan the image
    lastPinchLength; //helps determine if we are pinching in or out
    animation; //current animation handle
    wrapperRef;
    imageRef; //image element
    containerRef;
    isImageLoaded; //permits initial transform
    originalOverscrollBehaviorY; //saves the original overscroll-behavior-y value while temporarily preventing pull down refresh
    
    isTouchDisabledOnEvent(event) {
        return this.props.shouldDisableTouch(event);
    }

    //event handlers
    handleTouchStart = event => {
        if (this.props.touchDisabled  || this.isTouchDisabledOnEvent(event) || !this.isImageReady) {
            return;
        }
        this.cancelAnimation();
        const touches = event.touches;
        if (touches.length === 2) {
            this.lastPinchLength = getPinchLength(touches);
            this.lastPanPointerPosition = null;
        }
        else if (touches.length === 1) {
            this.lastPinchLength = null;
            this.pointerDown(touches[0]);
            tryCancelEvent(event); //suppress mouse events
        }
    }

    handleTouchMove = event => {
        if (this.props.touchDisabled || this.isTouchDisabledOnEvent(event) || !this.isImageReady) {
          return;
        }
        const touches = event.touches;
        if (touches.length === 2) {
            this.pinchChange(touches);

            //suppress viewport scaling on iOS
            tryCancelEvent(event);
        }
        else if (touches.length === 1) {
            const requestedPan = this.pan(touches[0]);

            if (!this.controlOverscrollViaCss) {
                //let the browser handling panning if we are at the edge of the image in
                //both pan directions, or if we are primarily panning in one direction
                //and are at the edge in that directino
                const overflow = imageOverflow(this.state, this.getTransform());
                const hasOverflowX = (requestedPan.left && overflow.left > 0) || (requestedPan.right && overflow.right > 0);
                const hasOverflowY = (requestedPan.up && overflow.top > 0) ||  (requestedPan.down && overflow.bottom > 0);

                if (!hasOverflowX && !hasOverflowY) {
                    //no overflow in both directions
                    return;
                }

                const panX = requestedPan.left || requestedPan.right;
                const panY = requestedPan.up || requestedPan.down;
                if (panY > 2 * panX && !hasOverflowY) {
                    //primarily panning up or down and no overflow in the Y direction
                    return;
                }

                if (panX > 2 * panY && !hasOverflowX) {
                    //primarily panning left or right and no overflow in the X direction
                    return;
                }

                tryCancelEvent(event);
            }
        }
    }

    handleTouchEnd = event => {
        if (this.props.touchDisabled || this.isTouchDisabledOnEvent(event) || !this.isImageReady) {
          return;
        }
        this.cancelAnimation();
        if (event.touches.length === 0 && event.changedTouches.length === 1) {
            if (this.lastPointerUpTimeStamp && this.lastPointerUpTimeStamp + DOUBLE_TAP_THRESHOLD > event.timeStamp) {
                const pointerPosition = getRelativePosition(event.changedTouches[0], this.containerRef);
                this.doubleClick(pointerPosition);
            }
            this.lastPointerUpTimeStamp = event.timeStamp;
            // tryCancelEvent(event); //suppress mouse events
        }

        //We allow transient +/-5% over-pinching.
        //Animate the bounce back to constraints if applicable.
        this.maybeAdjustCurrentTransform(ANIMATION_SPEED);
        return;
    }

    selection = true;

    handleMouseDown = event => {
        if (this.props.touchDisabled  || this.isTouchDisabledOnEvent(event) || !this.isImageReady) {
          return;
        }
        this.cancelAnimation();
        this.pointerDown(event);
    }

  handleMouseUp = event => {
    if (this.state.grabbing) {
      this.setState({ grabbing: false });
    }
    if (this.props.touchDisabled || !this.isImageReady) {
      return;
    }
  }

  mayIgnoreEvent = event => {
    if (this.props.touchDisabled  || this.isTouchDisabledOnEvent(event) || !this.isImageReady) {
      return;
    }
    return tryCancelEvent(event);
  };

      handleMouseHover = event => {
        if (this.isTouchDisabledOnEvent(event)) {
            if (!this.state.disableCursor) {
              this.setState({disableCursor: true});
            }
        } else if (this.state.disableCursor) {
          this.setState({ disableCursor: false });
        }
      }

  handleSelectionChange = event => {
        const selection = document.getSelection();
        if (selection.toString().length > 0) {
          let node = selection.baseNode;
          while (node != null) {
            if (node === this.containerRef) {
              if (!this.state.selecting) {
                this.setState({selecting: true});
              }
              return;
            }
            node = node.parentNode;
          }
        }
    if (this.state.selecting) {
      this.setState({selecting: false});
    }
  };
    handleMouseMove = event => {
        if (this.props.touchDisabled || !this.isImageReady || !this.state.grabbing) {
          return;
        }
        document.getSelection().empty();
        if (!event.buttons) {
              if (this.state.grabbing) {
                this.setState({ grabbing: false });
              }
            return;
        }
        this.pan(event)
    }



    handleMouseDoubleClick = event => {
        if (this.props.touchDisabled  || this.isTouchDisabledOnEvent(event) || !this.isImageReady) {
          return;
        }
        this.cancelAnimation();
        var pointerPosition = getRelativePosition(event, this.containerRef);
        this.doubleClick(pointerPosition);
    }

    handleMouseWheel = event => {
        if (this.props.touchDisabled || !this.isImageReady) {
          return;
        }
        this.cancelAnimation();
        const point = getRelativePosition(event, this.containerRef);
        const transform = this.getTransform();
        if (event.deltaY > 0) {
            if (transform.scale > getMinScale(this.state, this.props)) {
                this.zoomOut(point);
                tryCancelEvent(event);
            }
        } else if (event.deltaY < 0) {
            if (transform.scale < this.props.maxScale) {
                this.zoomIn(point);
                tryCancelEvent(event);
            }
        }
    }

    handleImageLoad = event => {
        this.debug('handleImageLoad');
        this.isImageLoaded = true;
        this.maybeHandleDimensionsChanged();

        const { onLoad } = React.Children.only(this.props.children);
        if (typeof onLoad === 'function') {
            onLoad(event);
        }
    }

    handleZoomInClick = () => {
        this.cancelAnimation();
        this.zoomIn();
    }

    handleZoomOutClick = () => {
        this.cancelAnimation();
        this.zoomOut();
    }

    handleWindowResize = () => this.maybeHandleDimensionsChanged();

    handleContainerRef = ref => {
      if (this.containerRef) {
        this.containerRef.removeEventListener('touchmove', this.handleTouchMove);
      }
      this.containerRef = ref;
      if (ref) {
        this.containerRef.addEventListener('touchmove', this.handleTouchMove, { passive: false });
      }
      const { ref: containerRefProp } = this.props;

      setRef(containerRefProp, ref);
    };

    handleRefWrapper = ref => {
        if (this.wrapperRef) {
            this.cancelAnimation();
        }

        this.wrapperRef = ref;
    };

      handleRefImage = ref => {
        this.imageRef = ref;
        const { ref: imageRefProp } = React.Children.only(this.props.children);
        setRef(imageRefProp, ref);
      };

    //actions
    pointerDown(clientPosition) {
      if (!this.state.grabbing) {
        this.setState({ grabbing: true });
      }
        this.lastPanPointerPosition = getRelativePosition(clientPosition, this.containerRef);
    }

    pan(pointerClientPosition) {
        if (!this.isTransformInitialized) {
            return;
        }

        if (!this.lastPanPointerPosition) {
            //if we were pinching and lifted a finger
            this.pointerDown(pointerClientPosition);
            return 0;
        }

        const transform = this.getTransform();
        const pointerPosition = getRelativePosition(pointerClientPosition, this.containerRef);
        const translateX = pointerPosition.x - this.lastPanPointerPosition.x;
        const translateY = pointerPosition.y - this.lastPanPointerPosition.y;
        this.lastPanPointerPosition = pointerPosition;

        const top = transform.top + translateY;
        const left = transform.left + translateX;
        this.constrainAndApplyTransform(top, left, transform.scale, 0, 0);

        return {
            up: translateY > 0 ? translateY : 0,
            down: translateY < 0 ? negate(translateY) : 0,
            right: translateX < 0 ? negate(translateX) : 0,
            left: translateX > 0 ? translateX : 0,
        };
    }

    doubleClick(pointerPosition) {
        const transform = this.getTransform();
        if (String(this.props.doubleTapBehavior).toLowerCase() === 'zoom') {
            if (transform.scale * (1 + OVERZOOM_TOLERANCE) < this.props.maxScale) {
              this.zoomIn(pointerPosition, ANIMATION_SPEED, 0.3);
            } else {
                this.zoom(getMinScale(this.state, transform), this.getCenter(), 0, ANIMATION_SPEED);
            }
        } else {
          //reset
          this.applyInitialTransform(ANIMATION_SPEED);
        }
    }

    pinchChange(touches) {
        const transform = this.getTransform();
        const length = getPinchLength(touches);
        let midpoint = getPinchMidpoint(touches);
        const relative = getRelativePosition(midpoint, this.wrapperRef);
        console.log(relative);
        const scale = this.lastPinchLength
            ? transform.scale * length / this.lastPinchLength //sometimes we get a touchchange before a touchstart when pinching
            : transform.scale;

        this.zoom(scale, midpoint, OVERZOOM_TOLERANCE);

        this.lastPinchLength = length;
    }

    getCenter() {
        return {
          x: this.state.containerDimensions.width / 2,
          y: this.state.containerDimensions.height / 2
        };
    }

    zoomIn(midpoint, speed = 0, factor = 0.1) {
        const transform = this.getTransform();
        midpoint = midpoint || this.getCenter();
        this.zoom(transform.scale * (1 + factor), midpoint, 0, speed);
    }

    zoomOut(midpoint) {
       const transform = this.getTransform();
        midpoint = midpoint || this.getCenter();
        this.zoom(transform.scale * 0.9, midpoint, 0);
    }

    getTransform = () => this.props.transform || this.state;

    zoom(requestedScale, containerRelativePoint, tolerance, speed = 0) {
        if (!this.isTransformInitialized) {
            return;
        }

        const { scale, top, left } = this.getTransform();
        const imageRelativePoint = {
            top: containerRelativePoint.y - top,
            left: containerRelativePoint.x - left,
        };

        const nextScale = this.getConstrainedScale(requestedScale, tolerance);
        const incrementalScalePercentage = (nextScale - scale) / scale;
        const translateY = imageRelativePoint.top * incrementalScalePercentage;
        const translateX = imageRelativePoint.left * incrementalScalePercentage;

        const nextTop = top - translateY;
        const nextLeft = left - translateX;

        this.constrainAndApplyTransform(nextTop, nextLeft, nextScale, tolerance, speed);
    }

    //compare stored dimensions to actual dimensions; capture actual dimensions if different
    maybeHandleDimensionsChanged = () => {
        clearTimeout(this.dimensionsTimer);
        const { onDimensionsChange } = this.props;
        if (this.isImageReady) {
            const containerDimensions = getContainerDimensions(this.containerRef);
            const imageDimensions = getDimensions(this.imageRef);

            if (!isEqualDimensions(containerDimensions, getDimensions(this.state.containerDimensions)) ||
                !isEqualDimensions(imageDimensions, getDimensions(this.state.imageDimensions))) {
                this.cancelAnimation();

                if (onDimensionsChange) {
                    onDimensionsChange(containerDimensions, imageDimensions);
                }
                //capture new dimensions
                this.setState({
                        containerDimensions,
                        imageDimensions
                    },
                    () => {
                        //When image loads and image dimensions are first established, apply initial transform.
                        //If dimensions change, constraints change; current transform may need to be adjusted.
                        //Transforms depend on state, so wait until state is updated.
                        if (!this.isTransformInitialized) {
                            this.applyInitialTransform();
                            console.log('apply', this.props.initialScale, this.props.initialScale == null
                              ? getAutofitScale(this.state.containerDimensions, this.state.imageDimensions)
                              : this.props.initialScale, this.state.imageDimensions);
                            this.isTransformInitialized = true;
                        } else {
                            this.maybeAdjustCurrentTransform();
                        }
                    }
                );
                this.debug(`Dimensions changed: Container: ${containerDimensions.width}, ${containerDimensions.height}, Image: ${imageDimensions.width}, ${imageDimensions.height}`);
            }
        }
        else {
            this.debug('Image not loaded');
        }
        if (this.props.dimensionsCheckInterval != null) {
          this.dimensionsTimer = setTimeout(this.maybeHandleDimensionsChanged, this.props.dimensionsCheckInterval);
        }
    }

    //transformation methods

    //Zooming and panning cause transform to be requested.
    constrainAndApplyTransform(requestedTop, requestedLeft, requestedScale, tolerance, speed = 0) {
        const requestedTransform = {
            top: requestedTop,
            left: requestedLeft,
            scale: requestedScale
        };
        this.debug(`Requesting transform: left ${requestedLeft}, top ${requestedTop}, scale ${requestedScale}`);

        //Correct the transform if needed to prevent overpanning and overzooming
        const transform = this.getCorrectedTransform(requestedTransform, tolerance) || requestedTransform;
        this.debug(`Applying transform: left ${transform.left}, top ${transform.top}, scale ${transform.scale}`);

        if (isEqualTransform(transform, this.getTransform())) {
            return false;
        }

        this.applyTransform(transform, speed);
        return true;
    }

    applyTransform({ top, left, scale }, speed) {
        if (speed > 0) {
            const frame = () => {
                const transform = this.getTransform();
                const translateY = top - transform.top;
                const translateX = left - transform.left;
                const translateScale = scale - transform.scale;

                this.nextAnimationCalllback = () => this.animation = requestAnimationFrame(frame);
                this.updateTransform({
                    top: snapToTarget(transform.top + (speed * translateY), top, 1),
                    left: snapToTarget(transform.left + (speed * translateX), left, 1),
                    scale: snapToTarget(transform.scale + (speed * translateScale), scale, 0.001),
                });
                //animation runs until we reach the target

            };
          this.animation = requestAnimationFrame(frame);
        } else {
            this.updateTransform({
              top,
              left,
              scale,
            });
        }
    }

    updateTransform = (nextTransform) => {
      const { onTransformChange } = this.props;
      const transform = this.getTransform();
      if (!transform || !isEqualTransform(nextTransform, transform)) {
        if (onTransformChange) {
          onTransformChange(nextTransform, { minScale: getMinScale(this.state, this.props), maxScale: this.props.maxScale, overflow: imageOverflow(this.state, nextTransform) });
        } else {
          this.setState(nextTransform);
        }
      }
    }

    //Returns constrained scale when requested scale is outside min/max with tolerance, otherwise returns requested scale
    getConstrainedScale(requestedScale, tolerance) {
        const lowerBoundFactor = 1.0 - tolerance;
        const upperBoundFactor = 1.0 + tolerance;

        return constrain(
            getMinScale(this.state, this.props) * lowerBoundFactor,
            this.props.maxScale * upperBoundFactor,
            requestedScale
        );
    }

    //Returns constrained transform when requested transform is outside constraints with tolerance, otherwise returns null
    getCorrectedTransform(requestedTransform, tolerance) {
        const scale = this.getConstrainedScale(requestedTransform.scale, tolerance);

        //get dimensions by which scaled image overflows container
        const negativeSpace = this.calculateNegativeSpace(scale);
        const overflow = {
            width: Math.max(0, negate(negativeSpace.width)),
            height: Math.max(0, negate(negativeSpace.height)),
        };

        //if image overflows container, prevent moving by more than the overflow
        //example: overflow.height = 100, tolerance = 0.05 => top is constrained between -105 and +5
        const { position, initialTop, initialLeft } = this.props;
        const { imageDimensions, containerDimensions } = this.state;
        const upperBoundFactor = 1.0 + tolerance;
        const top =
            overflow.height ? constrain(negate(overflow.height) * upperBoundFactor, overflow.height * upperBoundFactor - overflow.height, requestedTransform.top)
            : position === 'center' ? (containerDimensions.height - (imageDimensions.height * scale)) / 2
            : initialTop || 0;

        const left =
            overflow.width ? constrain(negate(overflow.width) * upperBoundFactor, overflow.width * upperBoundFactor - overflow.width, requestedTransform.left)
            : position === 'center' ? (containerDimensions.width - (imageDimensions.width * scale)) / 2
            : initialLeft || 0;

        const constrainedTransform = {
            top,
            left,
            scale
        };

        return isEqualTransform(constrainedTransform, requestedTransform)
            ? null
            : constrainedTransform;
    }

    //Ensure current transform is within constraints
    maybeAdjustCurrentTransform(speed = 0) {
        let correctedTransform;
        if (correctedTransform = this.getCorrectedTransform(this.getTransform(), 0)) {
            this.applyTransform(correctedTransform, speed);
        }
    }

    applyInitialTransform(speed = 0) {
        const { imageDimensions, containerDimensions } = this.state;
        const {
            position,
            initialScale,
            initialTop,
            initialLeft,
            transform,
            maxScale,
        } = this.props;

        const scale = initialScale == null
            ? getAutofitScale(containerDimensions, imageDimensions)
            : initialScale;

        const minScale = getMinScale(this.state, this.props);

        if (minScale > maxScale) {
            warning(false, 'minScale cannot exceed maxScale.');
            return;
        }
        if (scale < minScale || scale > maxScale) {
            warning(false, 'scale must be between minScale and maxScale.');
            return;
        }

        let initialPosition;
        if (position === 'center') {
            warning(initialTop == null, 'initialTop prop should not be supplied with position=center. It was ignored.');
            warning(initialLeft == null, 'initialLeft prop should not be supplied with position=center. It was ignored.');
            initialPosition = {
                top: (containerDimensions.width - (imageDimensions.width * scale)) / 2,
                left: (containerDimensions.height - (imageDimensions.height * scale)) / 2,
            };
        } else {
            initialPosition = {
                top: transform && transform.top != null ? transform.top : (initialTop || 0),
                left: transform && transform.left != null ? transform.left : (initialLeft || 0),
            };
        }

        this.constrainAndApplyTransform(initialPosition.top, initialPosition.left, scale, 0, speed);
    }

    //lifecycle methods
    render() {
        const {
          transform,
          initialTop,
          initialLeft,
          initialScale,
          minScale,
          maxScale,
          shouldDisableTouch,
          debug,
          dimensionsCheckInterval,
          onTransformChange,
          onDimensionsChange,
          position,
          rightClickDisabled,
          doubleTapBehavior,
          touchDisabled,
            children,
          style,
          ...rest
        } = this.props;
        const childElement = React.Children.only(children);

        const overflow = imageOverflow(this.state, this.getTransform());
        const touchAction = this.controlOverscrollViaCss
            ? browserPanActions(this.state, this.getTransform()) || 'none'
            : undefined;

        const containerStyle = {
          width: '100%',
          height: '100%',
          overflow: 'hidden',
          ...(!this.state.disableCursor && !this.state.selecting &&  {
          WebkitTouchCallout: 'none',
          WebkitUserSelect: 'none',
          KhtmlUserSelect: 'none',
          MozUserSelect: 'none',
          MsUserSelect: 'none',
          userSelect: 'none',
              cursor: // overflow.top === 0 && overflow.left === 0 && overflow.right === 0 && overflow.bottom === 0
                // ? 'auto'
                /* :*/ this.state.grabbing ? 'grabbing' : 'grab',
          }),
          touchAction: touchAction,
          position: 'relative',
          ...style,
        };

        return (
            <div
                {...rest}
                onTouchStart={this.handleTouchStart}
                onTouchEnd={this.handleTouchEnd}
                onMouseDown={this.handleMouseDown}
                onMouseUp={this.handleMouseUp}
                onDoubleClick={this.handleMouseDoubleClick}
                onWheel={this.handleMouseWheel}
                onMouseMove={this.handleMouseHover}
                onDragStart={this.mayIgnoreEvent}
                ref={this.handleContainerRef}
                style={containerStyle}
            >
                <div
                  ref={this.handleRefWrapper}
                  style={imageStyle({ ...this.state, ...this.getTransform() })}
                  {...rightClickDisabled && { onContextMenu: tryCancelEvent }}
                >
                    {React.cloneElement(childElement, {
                        onLoad: this.handleImageLoad,
                        ref: this.handleRefImage,
                        style: {
                          ...childElement.props.style,
                          ...(!this.isImageReady && { visibility: 'hidden' }),
                        }
                    })}
                    <div style={{ clear: 'both' }} />
                </div>
               {debug && <DebugView {...this.state} {...this.getTransform()} overflow={overflow} />}
            </div>
        );
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.initialScale !== prevState.initialScale ||
            nextProps.initialTop !== prevState.initialTop ||
            nextProps.initialLeft !== prevState.initialLeft ||
            nextProps.position !== prevState.position ||
            nextProps.transform !== prevState.transform) {
            return {
              position: nextProps.position,
              initialScale: nextProps.initialScale,
              initialTop: nextProps.initialTop,
              initialLeft: nextProps.initialLeft,
              scale: nextProps.transform != null && nextProps.transform.scale != null ? nextProps.transform.scale : prevState.scale,
              top: nextProps.transform != null && nextProps.transform.top != null ? nextProps.transform.top : prevState.top,
              left: nextProps.transform != null && nextProps.transform.left != null ? nextProps.transform.left : prevState.left,
            };
        } else {
            return null;
        }
    }

    body() {
        return (document.body || document.getElementsByTagName('body')[0]);
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleWindowResize);
        this.maybeHandleDimensionsChanged();
        this.body().addEventListener('mousemove', this.handleMouseMove);
        document.addEventListener('selectionchange', this.handleSelectionChange);
        /* TODO
        document.addEventListener('selectstart', event => {
          const range = document.createRange();
          range.selectNodeContents(event.target);
          var rects = range.getClientRects();
          if (rects.length > 0) {
            console.log("Text node rect: ", rects[0], rects);
          }
        });
        */
    }

    componentDidUpdate(prevProps, prevState) {
        const nextAnimationCalllback = this.nextAnimationCalllback;
        this.nextAnimationCalllback = undefined;
        if (nextAnimationCalllback) {
          nextAnimationCalllback();
        }
        this.maybeHandleDimensionsChanged();
    }

    componentWillUnmount() {
        this.cancelAnimation();
        this.containerRef.removeEventListener('touchmove', this.handleTouchMove);
        window.removeEventListener('resize', this.handleWindowResize);
        this.body().removeEventListener('mousemove', this.handleMouseMove);
        document.removeEventListener('selectionchange', this.handleSelectionChange);
        clearTimeout(this.dimensionsTimer);
    }

    get isImageReady() {
        return this.isImageLoaded || (this.imageRef && this.imageRef.tagName !== 'IMG');
    }

    isTransformInitialized = false;

    get controlOverscrollViaCss() {
        return CSS && CSS.supports('touch-action', 'pan-up');
    }

    calculateNegativeSpace(scale) {
        //get difference in dimension between container and scaled image
        const { containerDimensions, imageDimensions } = this.state;
        const width = containerDimensions.width - (scale * imageDimensions.width);
        const height = containerDimensions.height - (scale * imageDimensions.height);
        return {
            width,
            height
        };
    }

    cancelAnimation() {
        if (this.animation) {
            cancelAnimationFrame(this.animation);
        }
    }

    debug(message) {
        if (this.props.debug) {    
            console.log(message);
        }
    }
}

Scalable.defaultProps = {
    dimensionsCheckInterval: 100,
    minScale: null,
    maxScale: 1,
    position: 'topLeft',
    doubleTapBehavior: 'reset',
    shouldDisableTouch: defaultShouldDisableTouch,
};

const transformShape = PropTypes.shape({ scale: PropTypes.number, top: PropTypes.number, left: PropTypes.number });
Scalable.propTypes = {
    children: PropTypes.element.isRequired,
    minScale: PropTypes.number,
    maxScale: PropTypes.number,
    transform: transformShape,
    onTransformChange: PropTypes.func,
    onDimensionsChange: PropTypes.func,
    rightClickDisabled: PropTypes.bool,
    position: PropTypes.oneOf(['topLeft', 'center']),
    doubleTapBehavior: PropTypes.oneOf(['reset', 'zoom']),
    touchDisabled: PropTypes.bool,
    initialScale: PropTypes.number,
    initialTop: PropTypes.number,
    initialLeft: PropTypes.number,
    shouldDisableTouch: PropTypes.func,
};


export default Scalable;