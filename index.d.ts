import { HTMLProps, ReactElement, FunctionComponent } from 'react';

export interface TransformState {
  top: number;
  left: number;
  scale: number;
}

export interface BoundaryState {
  minScale: number,
  maxScale: number,
  overflow: {
    top: number,
    left: number,
    right: number,
    bottom: number,
  },
}

export interface TransformChangeEventHandler {
  (transform: TransformState, boundary: BoundaryState): void;
}

export interface DimensionsState {
  width: number;
  height: number;
}

export interface DimensionsChangeEventHandler {
  (container: DimensionsState, content: DimensionsState): void;
}

export interface ScalableProps extends HTMLProps<HTMLDivElement> {
  children: ReactElement<any>,
  minScale?: number,
  maxScale?: number,
  transform?: TransformState | null,
  onTransformChange?: TransformChangeEventHandler,
  onDimensionsChange?: DimensionsChangeEventHandler,
  position?: 'topLeft' | 'center',
  dimensionsCheckInterval?: number,
  rightClickDisabled?: boolean,
  doubleTapBehavior?: 'reset' | 'zoom',
  touchDisabled?: boolean,
  initialTransform?: Partial<TransformState>,
  shouldDisableTouch?: (event: Event) => boolean,
}

export const Scalable: FunctionComponent<ScalableProps>;

export default Scalable;